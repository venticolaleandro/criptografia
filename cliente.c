/*
Creado por Jorge Duran (ganchito55@gmail.com)
The MIT License (MIT)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

 */



#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <netdb.h>

#include <openssl/rand.h>
#include <openssl/conf.h>
#include <openssl/evp.h>

#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/err.h>
#include <stdio.h>
#include <string.h>
#include <gtk/gtk.h>

#define PUERTO 5555
#define TAM_BUFFER 10000
#define AES_M 1
#define RSA_M 2
#define SHA_M 3
#define TAM_TEXTO 1000
#define TAM_TEXTO_CIFRADO 1500 //El texto cifrado puede ocupar más que en texto plano
#define RSA_LONG 4096
#define RSA_EXP 65537 //Es mucho mas seguro que exponentes pequeños como 3 y 17, y solo tiene 2 1s en binario

int s;/* connected socket descriptor */

int menuAlgortimos(int s);
int clienteTCP(char *ip);
void funcionAES(GtkWidget * widget, GtkWidget * entrytext);
int encrypt(unsigned char *plaintext, int plaintext_len, unsigned char *key, unsigned char *iv, unsigned char *ciphertext);
void handleErrors(void);
void funcionRSA(int s);
void funcionSHA(int s);

//Funcion  a la que se llama cuando se da click en aceptar

void callback(GtkWidget *widget, gpointer data){
    g_print("Esto es una prueba :D \n");
}

/* Another callback */
void destroy( GtkWidget *widget, gpointer   data ){
        gtk_main_quit ();
}

gint delete_event( GtkWidget *widget, GdkEvent  *event, gpointer   data ){
        /* If you return FALSE in the "delete_event" signal handler,
         * GTK will emit the "destroy" signal. Returning TRUE means
         * you don't want the window to be destroyed.
         * This is useful for popping up 'are you sure you want to quit?'
         * type dialogs. */

        g_print ("El boton cerrar ha sido presionado. \n");

        /* Change TRUE to FALSE and the main window will be destroyed with
         * a "delete_event". */

        return TRUE;
}

void fAES(){
    GtkWidget *ventana;
	GtkWidget *tabla;
	GtkWidget *texto;
	GtkWidget *botonA;
	GtkWidget *botonC;
	GtkWidget *label;
	
	//gtk_init ();
	
	//creando la ventana 
	ventana=gtk_window_new(GTK_WINDOW_TOPLEVEL);
	
	gtk_window_set_title (GTK_WINDOW (ventana), "Encriptacion por AES");
	
	//cambiar los bordes.
	gtk_container_set_border_width(GTK_CONTAINER(ventana),20);
	
	//crear tabla
	tabla=gtk_table_new(6,3,TRUE);
	//label
	label=gtk_label_new("Ingrese el mensaje a encriptar :");
	
	gtk_table_attach_defaults(GTK_TABLE(tabla),label,0,3,0,1);
	
	//Ingresar texto
	
	texto=gtk_entry_new();
	
	//Agregando caja de texto a tabla
	gtk_table_attach_defaults(GTK_TABLE(tabla),texto,0,3,1,4);
	
	//Creando el boton aceptar.
	botonA=gtk_button_new_with_label("Aceptar");
	
	//Creando el boton cancelar.
	botonC=gtk_button_new_with_label("cancelar");
	
    g_signal_connect(G_OBJECT(botonA),"clicked", G_CALLBACK(funcionAES),texto);

    g_signal_connect(G_OBJECT(botonC),"clicked", G_CALLBACK(destroy),NULL);

	//Agregando el boton
	gtk_table_attach_defaults(GTK_TABLE(tabla),botonA,0 ,1 , 4, 5);
	
	//Agregando el boton
	gtk_table_attach_defaults(GTK_TABLE(tabla),botonC,2 , 3 , 4, 5);
	
	//Colocando la tabla en la ventana principal.
    gtk_container_add(GTK_CONTAINER (ventana), tabla);
	
	gtk_widget_show (label);
	gtk_widget_show (texto);
	gtk_widget_show (botonA);
	gtk_widget_show (botonC);
	gtk_widget_show (tabla);
	gtk_widget_show (ventana);
	  
}        

void fRSA(){

}

void fHASH(){

}        

void seleccion(GtkWidget *widget,GtkWidget *combobox){    

    gchar *sel=gtk_combo_box_get_active_id(combobox);
    switch(sel[0]){
        case 'a':{
            fAES();
        }break;
        case 'b':{   
            fRSA();
        }break;
        case 'c':{
            fHASH();
        }break;                 
        default:{
        }
    }           
        
}


//Usado para mensajes de errores
//XD

void printer(GtkWidget *mensaje){
	GtkWidget *dialog;
	GtkWidget *window;
	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	dialog = gtk_message_dialog_new(GTK_WINDOW(window),GTK_DIALOG_DESTROY_WITH_PARENT,GTK_MESSAGE_INFO,GTK_BUTTONS_OK,gtk_label_get_text(mensaje));
	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);		
}

int clienteTCP(char *ip) {

    struct sockaddr_in servaddr_in; /* for server socket address */

    memset((char *) &servaddr_in, 0, sizeof (struct sockaddr_in));

    /* Set up the peer address to which we will connect. */
    servaddr_in.sin_family = AF_INET;
    servaddr_in.sin_addr.s_addr = inet_addr(ip);
    servaddr_in.sin_port = htons(PUERTO);

    /* Create the socket. */
    s = socket(AF_INET, SOCK_STREAM, 0);
    if (s == -1) {
        perror("Cliente");
        fprintf(stderr, "unable to create socket\n");
        exit(1);
    }
    /* Try to connect to the remote server at the address
     * which was just built into peeraddr.
     */
    if (connect(s, (const struct sockaddr *) &servaddr_in, sizeof (struct sockaddr_in)) == -1) {
        perror("Cliente");
        fprintf(stderr, "unable to connect to remote\n");
        exit(1);
    }
    return s;
}	

void funcionAES(GtkWidget * widget, GtkWidget * entrytext) {
    int i, enviados;
    unsigned char key[32]; //Clave
    unsigned char iv[16]; //Vector de inicializacion
    unsigned char ciphertext[TAM_TEXTO_CIFRADO];
    char texto[TAM_TEXTO];
    int ciphertextLen;
    FILE * f;
    GtkWidget *label;
    gchar *temp_texto;
    char buffer[TAM_BUFFER];

    strcpy(buffer, "AES");

    if (send(s, buffer, strlen(buffer), 0) != strlen(buffer)) {
        label=gtk_label_new("Error al enviar el algoritmo");
        printer(label);
    }
    memset(buffer, 3, sizeof (buffer)); //Limpiamos el buffer
    enviados = recv(s, buffer, strlen(buffer), 0);
    if (enviados == -1){
        label=gtk_label_new("Error al recibir la confirmacion del algoritmo");
        printer(label);
    } 
    else buffer[enviados] = '\0';

    //Genemos clave y vi aleatorios
    if (!RAND_bytes(key, sizeof (key))) {
        label=gtk_label_new("Error generando la clave");
        return;
    }
    if (!RAND_bytes(iv, sizeof (iv))) {
        label=gtk_label_new("Error generando el vector de inicializacion");
        printer(label);
        return;
    }

    /*Cargamos las bibliotecas */
    ERR_load_crypto_strings();
    OpenSSL_add_all_algorithms();
    OPENSSL_config(NULL);

    for (i = 0; i < 32; i++) {
        printf("%x", key[i]);
    }
    
    temp_texto=gtk_entry_get_text(entrytext);
    strcpy(texto,temp_texto);

    
    ciphertextLen = encrypt(texto, strlen(texto), key, iv, ciphertext); //Pasamos el texto y su tamaño, la clave, el iv y nos devuelve el texto cifrado y la longitud del mismo
    
    
    //BIO_dump_fp(stdout, (const char *) ciphertext, ciphertextLen);

    //Ahora enviamos el texto cifrado por el socket
    enviados = send(s, ciphertext, sizeof (char)*ciphertextLen, 0);
    if (enviados != ciphertextLen){
        label=gtk_label_new("Error al enviar el texto cifrado");
        printer(label);
    }
    label=gtk_label_new("Informacion cifrada enviada");
    printer(label);
    //Enviamos el vector de inicializacion por el socket
    //Se envia por esto: https://en.wikipedia.org/wiki/Initialization_vector
    //Mas informacion http://crypto.stackexchange.com/questions/732/why-use-an-initialization-vector-iv
    enviados = send(s, iv, sizeof (iv), 0);
    if (enviados != sizeof (iv)){
        label=gtk_label_new("Error al enviar el vector de inicializacion");
        printer(label);    
    } 
    label=gtk_label_new("Vector de inicializacion enviado");
    printer(label);

    f = fopen("clave.key", "wb");
    fwrite(key, sizeof (key), 1, f);
    fclose(f);
    label=gtk_label_new("Archivo con la clave generada, envialo al receptor por un canal seguro");
    printer(label);
    gtk_main_quit();
}

int encrypt(unsigned char *plaintext, int plaintext_len, unsigned char *key,
        unsigned char *iv, unsigned char *ciphertext) {
    EVP_CIPHER_CTX *ctx;

    int len;

    int ciphertext_len;

    /* Create and initialise the context */
    if (!(ctx = EVP_CIPHER_CTX_new())) handleErrors();

    /* Initialise the encryption operation. IMPORTANT - ensure you use a key
     * and IV size appropriate for your cipher
     * In this example we are using 256 bit AES (i.e. a 256 bit key). The
     * IV size for *most* modes is the same as the block size. For AES this
     * is 128 bits */
    if (1 != EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv))
        handleErrors();

    /* Provide the message to be encrypted, and obtain the encrypted output.
     * EVP_EncryptUpdate can be called multiple times if necessary
     */
    if (1 != EVP_EncryptUpdate(ctx, ciphertext, &len, plaintext, plaintext_len))
        handleErrors();
    ciphertext_len = len;

    /* Finalise the encryption. Further ciphertext bytes may be written at
     * this stage.
     */
    if (1 != EVP_EncryptFinal_ex(ctx, ciphertext + len, &len)) handleErrors();
    ciphertext_len += len;

    /* Clean up */
    EVP_CIPHER_CTX_free(ctx);

    return ciphertext_len;
}

void handleErrors(void) {
    ERR_print_errors_fp(stderr);
    abort();
}



int main( int   argc, char** argv[] ){
	
	    /* GtkWidget is the storage type for widgets */
        GtkWidget *window;
        GtkWidget *button;
        GtkWidget *button2;
        GtkWidget *table;
        GtkWidget *combo;
        GtkWidget *label;
        GtkWidget *msjlabel;
        /* This is called in all GTK applications. Arguments are parsed
         * from the command line and are returned to the application. */
		int retorno;
		if (argc < 2) {
        msjlabel=gtk_label_new("El software debe ser llamado como cliente IPdelServidor: cliente 192.168.1.55");
        printer(msjlabel);
        return 2;
		}
		s = clienteTCP(argv[1]);
		//retorno = menuAlgortimos(s);
    
        gtk_init (&argc, &argv);

        /* create a new window */
        window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
        
        /*Creando una tabla*/

        table = gtk_table_new(5,2,TRUE);

        /*  */
        g_signal_connect (G_OBJECT (window), "delete_event", G_CALLBACK (destroy), NULL);

        /*  */
        g_signal_connect (G_OBJECT (window), "destroy",G_CALLBACK (delete_event), NULL);

        //Cambia el tamaño de los bordes de la ventana
        gtk_container_set_border_width (GTK_CONTAINER (window), 20);

        //Colocando la tabla en la ventana principal.
        gtk_container_add(GTK_CONTAINER (window), table);
        
        //Creacion del label
        label=gtk_label_new("Seleccione una opcion : ");

        gtk_table_attach_defaults(GTK_TABLE(table),label,0,1,0,2);

		//Creando un combobox
        
        combo= gtk_combo_box_text_new();
        gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(combo),"a","1 - Encriptacion AES");
        gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(combo),"b","2 - Encriptacion RSA");
        gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(combo),"c","3 - Encriptacion HASH");
        
        gtk_table_attach_defaults(GTK_TABLE(table),combo,0,2,2,3);
        gtk_widget_show(label);
        // Mostrando el combobox
        gtk_widget_show(combo);	
	
        // Creando el primer boton
        
        button= gtk_button_new_with_label("Aceptar");

        // cuando se pulse el primer boton va a llamar la ventana

        g_signal_connect(G_OBJECT(button),"clicked", G_CALLBACK(seleccion),combo);

        //Colocando boton en tabla pocision superior izquierda
        
        gtk_table_attach_defaults(GTK_TABLE (table),button, 0, 1, 4, 5);

        // Visualizando el boton
        
        gtk_widget_show(button);
        
        //segundo boton

        button2=gtk_button_new_with_label("Cancelar");

        //callback asociado al boton cancelar
        
        g_signal_connect(G_OBJECT(button2),"clicked", G_CALLBACK(destroy),NULL);

        //Colocando boton 2 en tabla
        
        gtk_table_attach_defaults(GTK_TABLE(table),button2,1 ,2 , 4, 5);

        //Visualizando boton 2
        
        gtk_widget_show(button2);

        // Muestra la tabla 2x2

        gtk_widget_show(table);

        // Muestra la ventana
        gtk_widget_show (window);

        /* All GTK applications must have a gtk_main(). Control ends here
         * and waits for an event to occur (like a key press or
         * mouse event). */
        gtk_main ();

        return 0;
}
